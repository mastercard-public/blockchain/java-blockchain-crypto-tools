# Java Crypto SDK for Mastercard Blockchain

### Introduction
This project implements a cryptography module specifically for the Mastercard Blockchain.

### Build
Run command: 
   * `mvn package`
This will generated two jar files:
   * target/blockchain-crypto-1.0-SNAPSHOT.jar
   * target/blockchain-crypto-1.0-SNAPSHOT-jar-with-dependencies.jar

### Test
Run command:
   * `java -jar target/blockchain-crypto-1.0-SNAPSHOT-jar-with-dependencies.jar` 
 
### Doc
Run command:
   * `mvn javadoc:javadoc`
The generated document is located in `target/site/apidocs/index.html`.
