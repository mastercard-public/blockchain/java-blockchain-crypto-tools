package com.mastercard.blockchain.model;

import java.util.*;

import com.mastercard.blockchain.crypto.HashUtil;
import com.mastercard.blockchain.crypto.Base58;
import com.mastercard.blockchain.crypto.ByteUtil;

public class MerkleTree {

  // transaction List
  List<String> transactionList;

  // Merkle Root
  String root;
  
  /**
   * constructor
   */
  public MerkleTree() {
    this.transactionList = new ArrayList<String>();
    this.root = "";
  }

  /**
   * Get Root
   * @return root
   */
  public String getRoot() {
    return this.root;
  }

  /**
   * init a merkle tree with transaction list, and set root
   * @param txList - input transactions
   */
  public void initTree(List<String> txList) {

    if (txList == null || txList.size() == 0) {
      return;
    }

    for (int i = 0; i < txList.size(); i++) {
      this.transactionList.add(txList.get(i));
    }
    
    List<String> newTxList = upTree(this.transactionList);
    while (newTxList.size() != 1) {
      newTxList = upTree(newTxList);
    }
    
    this.root = newTxList.get(0);
  }

  /**
   * add a new transaction into existing merkle tree
   * @param tx - new transaction
   */
  public void addTransaction(String tx) {

    this.transactionList.add(tx);

    // re-caculate the merkle root
    List<String> newTxList = upTree(this.transactionList);
    while (newTxList.size() != 1) {
      newTxList = upTree(newTxList);
    }
    
    this.root = newTxList.get(0);
  }

  /**
   * verify whether the target transaction included in merkle tree or not.
   * @param tx - target transaction
   * @return true/false
   */
  public boolean validateTransaction(String tx) {
    boolean found = false;
    for (int i=0; i < this.transactionList.size(); i++) {
      String tx2 = this.transactionList.get(i);
      if ( tx2.equals(tx) ) {
        found = true;
        break;
      }
    }

    if (!found) {
      return false;
    }

    // now need to verify hash to the merkle root.
    // Note: in order to fast the process, one needs to save intermediate nodes of merkle tree
    List<String> newTxList = upTree(this.transactionList);
    while (newTxList.size() != 1) {
      newTxList = upTree(newTxList);
    }
    
    String newRoot = newTxList.get(0);
    return newRoot.equals(this.root);
  }

  
  /**
   * return Node Hash List.
   * @param tempTxList
   * @return list of hashes above one level
   */
  private List<String> upTree(List<String> tempTxList) {

    List<String> newTxList = new ArrayList<String>();
    int index = 0;
    while (index < tempTxList.size()) {
      // left
      String left = tempTxList.get(index);
      index++;

      // right
      String right = "";
      if (index != tempTxList.size()) {
        right = tempTxList.get(index);
      }

      // hash value
      String hashValue = doubleSha256(left + right);
      newTxList.add(hashValue);
      index++;
    }
    
    return newTxList;
  }
  
  /**
   * Return double SHA256 as string
   * @param str
   * @return hash
   */
  private String doubleSha256(String str) {
    byte[] data = str.getBytes();

    byte[] x1 = HashUtil.sha256(data);
    byte[] x2 = HashUtil.sha256(x1);

    return Base58.encode(x2);
  }
}