package com.mastercard.blockchain.model;

import java.util.HashMap;
import java.util.Map;

public enum AddressPrefix {
    Alpha(23), 
    Personal(55), 
    Device(30),
    Entity(33),
    Application(23),
    Consensus(28),
    Signatory(63),
    Audit(80),
    Witness(50);
 
    AddressPrefix(final int newValue) {
        this.value = newValue;
    }
 
    private final int value;
    public int getValue() {
        return value;
    }
 
    private static Map map = new HashMap<>();
    static {
        for (AddressPrefix p : AddressPrefix.values()) {
            map.put(p.value, p);
        }
    }
 
    public static AddressPrefix typeOf(int p) {
        return (AddressPrefix)map.get(p);
    }
}