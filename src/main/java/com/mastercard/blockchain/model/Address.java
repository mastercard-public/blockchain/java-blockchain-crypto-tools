package com.mastercard.blockchain.model;

public class Address {
	public String id;
	public byte[] publicKey;

	public String wif;
	public byte[] privateKey;
}