package com.mastercard.blockchain.test;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.*;
import javax.crypto.SecretKey;

import com.mastercard.blockchain.crypto.Base58;
import com.mastercard.blockchain.crypto.PublicKeyUtil;
import com.mastercard.blockchain.crypto.BlockCipherUtil;
import com.mastercard.blockchain.crypto.ByteUtil;
import com.mastercard.blockchain.crypto.HashUtil;
import com.mastercard.blockchain.model.Address;
import com.mastercard.blockchain.model.AddressPrefix;
import com.mastercard.blockchain.model.MerkleTree;
import com.mastercard.blockchain.api.ChainUtil;

public class TestChain {

	public static boolean test_base58() {

		byte[] data = ByteUtil.generateRandomArray(16);

		String s = Base58.encode(data);
		byte[] decoded = Base58.decode(s);
		System.out.println("\tencoded: " + s);

		return Arrays.equals(data, decoded);
	}

	public static boolean test_hash() {
		try {
			String message = "hello, the world!";
			byte[] data = message.getBytes("UTF-8"); 
			byte[] sha1 = HashUtil.sha1(data);
			byte[] sha256 = HashUtil.sha256(data);

			System.out.println("\tmessage: " + message);
			System.out.println("\tsha1: " + ByteUtil.bytesToHexString(sha1));
			System.out.println("\tsha256: " + ByteUtil.bytesToHexString(sha256));
		} catch(Exception ex) {
			ex.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean test_cipher(){

		byte[] iv = ByteUtil.generateRandomArray(16);
		byte[] key = ByteUtil.generateRandomArray(16);
		byte[] plaintext = ByteUtil.generateRandomArray(32);
		byte[] ciphertext = BlockCipherUtil.aesCfbEncrypt(iv, key, plaintext);
		byte[] decrypted = BlockCipherUtil.aesCfbDecrypt(iv, key, ciphertext);

		System.out.println("\tplaintext: " + Base58.encode(plaintext));
		System.out.println("\tdecrypted: " + Base58.encode(decrypted));
		return Arrays.equals(plaintext, decrypted);
	}

	public static boolean test_public_key() {
		KeyPair pair = PublicKeyUtil.generateKeyPair();
		byte[] publicKeyBytes = PublicKeyUtil.getPublicKeyBytes(pair);
		byte[] privateKeyBytes = PublicKeyUtil.getPrivateKeyBytes(pair);

		System.out.println("\tpublic key:  " + Base58.encode(publicKeyBytes));
		System.out.println("\tprivate key: " + Base58.encode(privateKeyBytes));

		// convert back
		PublicKey publicKey2 = PublicKeyUtil.bytesToPublicKey(publicKeyBytes);
		PrivateKey privateKey2 = PublicKeyUtil.bytesToPrivateKey(privateKeyBytes);

		System.out.println("\tpublic key2: " + Base58.encode(publicKey2.getEncoded()));
		System.out.println("\tprivate key2:" + Base58.encode(privateKey2.getEncoded()));

		boolean success = publicKey2.equals(pair.getPublic()) && privateKey2.equals(pair.getPrivate());
		if (!success) {
			System.out.println("key pair generation error!");
			return false;
		}

		// get publick key from private key
		PublicKey publicKey3 = PublicKeyUtil.generatePubKeyFromPrivKey(privateKey2);
		success = publicKey2.equals(publicKey3);
		if (!success) {
			System.out.println("priv key to pub key error!");
			return false;
		}


		// test signature
		byte[] plaintext = ByteUtil.generateRandomArray(32);
		byte[] signature = PublicKeyUtil.sign(plaintext, privateKey2);
		if (signature == null) {
			return false;
		}

		return PublicKeyUtil.verify(plaintext, signature, publicKey2);
	}

	public static boolean test_ecdh() {

		KeyPair alice = PublicKeyUtil.generateKeyPair();
		KeyPair bob   = PublicKeyUtil.generateKeyPair();

		byte[] secret_alice = PublicKeyUtil.computeKey(alice.getPrivate(), bob.getPublic());
		byte[] secret_bob   = PublicKeyUtil.computeKey(bob.getPrivate(), alice.getPublic());
		if (secret_alice == null || secret_bob == null) {
			System.out.println("compute shared secret key failed");
			return false;
		}

		System.out.println(Arrays.toString(secret_alice));
		System.out.println(Arrays.toString(secret_bob));

		return Arrays.equals(secret_alice, secret_bob);
	}

	public static boolean test_chain_address() {

		Address addr = ChainUtil.generateAddress(AddressPrefix.Entity);
		if (addr == null) {
			return false;
		}
		
		System.out.println("\taddress created: ");
		System.out.println("\t\tid:  " + addr.id);
		System.out.println("\t\twif: " + addr.wif);
		System.out.println("\t\tpublicKey : len=" + addr.publicKey.length + ", " + Base58.encode(addr.publicKey));
		System.out.println("\t\tprivateKey: len=" + addr.privateKey.length + ", " + Base58.encode(addr.privateKey));

		if (!ChainUtil.verifyAddress(addr.id, addr.publicKey)) {
			System.out.println("\tverify address ... failed.");
			return false;
		}

		byte[] plaintext = ByteUtil.generateRandomArray(32);
		byte[] privateKey = addr.privateKey;
		byte[] signature = ChainUtil.sign(plaintext, privateKey);
		if (signature == null) {
			System.out.println("\tsignature gen/verify ... failed.");
			return false;
		}

		byte[] publicKey = addr.publicKey;
		return ChainUtil.verify(plaintext, signature, publicKey);
	}


	public static boolean test_merkle_tree() {

		int targetEntry = 283;
		int allEntries = 2019;

		byte[] N = ByteUtil.integerToBytes(targetEntry);
		byte[] hash = ChainUtil.hash256(N);

		MerkleTree merkle = new MerkleTree();

		List<String> txList = new ArrayList<String>();
		for (Integer i=0; i < 100; i++) {
			String tx = new String( "tx-" + i.toString());
			txList.add(tx);
		}
		merkle.initTree(txList);

		String targetTx = "tx-50";
		return merkle.validateTransaction(targetTx);
	}

	public static void main(String[] args) {
		System.out.println("TEST Base58..." + test_base58());
		System.out.println("TEST Hash..." + test_hash());
		System.out.println("TEST AES128/CFB/NoPadding..." +  test_cipher());
		System.out.println("TEST PUBKEY EC..." + test_public_key());
		System.out.println("TEST ECDH..." + test_ecdh());
		System.out.println("TEST Chain Generate Address..." + test_chain_address());
		System.out.println("TEST Chain merkle tree..." + test_merkle_tree());
	}

}
