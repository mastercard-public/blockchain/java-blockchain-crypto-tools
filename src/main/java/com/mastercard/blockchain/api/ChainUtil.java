package com.mastercard.blockchain.api;

import java.security.MessageDigest;
import java.util.Arrays;
import java.security.SecureRandom;

import java.security.KeyPairGenerator;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EllipticCurve;
import java.security.spec.ECGenParameterSpec;

import java.security.KeyFactory;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;

import java.security.Signature;



import com.mastercard.blockchain.crypto.Base58;
import com.mastercard.blockchain.crypto.PublicKeyUtil;
import com.mastercard.blockchain.crypto.BlockCipherUtil;
import com.mastercard.blockchain.crypto.HmacUtil;
import com.mastercard.blockchain.crypto.ByteUtil;
import com.mastercard.blockchain.crypto.HashUtil;
import com.mastercard.blockchain.model.Address;
import com.mastercard.blockchain.model.AddressPrefix;

public class ChainUtil {

    public final static byte VERSION_PRIVATE_KEY = (byte)128;

    /**
     * Double SHA-256.
     *
     * @param data - input the bytes to hash
     * @return 32-byte hash
     */
    public static byte[] hash256(byte[] data) {
        byte[] x1 = HashUtil.sha256(data);
        byte[] x2 = HashUtil.sha256(x1);
        return x2;
    }

    /**
     * SHA-256 first, then SHA-1
     *
     * @param data - input the bytes to hash
     * @return 20-byte hash
     */
    public static byte[] hash160(byte[] data) {
        byte[] x1 = HashUtil.sha256(data);
        byte[] x2 = HashUtil.sha1(data);
        return x2;
    }

    /**
     * Generate 4-byte checksum
     *
     * @param data - input the bytes to checksum
     * @return checksum
     */
    private static byte[] checksum(byte[] data) {
        // double SHA-256
        byte[] h = hash256(data);

        // slice the first 4 bytes
        byte[] slice = Arrays.copyOfRange(h, 0, 4);
        return slice;
    }

    /**
     * Generate Pin
     *
     * @param hash256 -  hash256 value
     * @return Pin in string
     */
    public static String generatePin(byte[] hash256) {
        // validate input
        if (hash256.length != 32) {
            return null;
        }

        byte[] hash160 = hash160(hash256);

        byte[] keyHash = new byte[1 + hash160.length];
        keyHash[0] = 32;
        System.arraycopy(hash160, 0, keyHash, 1, hash160.length);

        byte[] checksum = checksum(keyHash);

        byte[] raw = new byte[keyHash.length + checksum.length];
        System.arraycopy(keyHash, 0, raw, 0, keyHash.length);
        System.arraycopy(checksum, 0, raw, keyHash.length, checksum.length);

        return Base58.encode(raw);
    }

    /**
     * Generate chain address from public key
     *
     * @param type - address prefix
     * @param publicKey - public key
     * @return based 58 encoded address
     */
    public static String encodeAddress(AddressPrefix type, byte[] publicKey) {
        byte[] hash160 = hash160(publicKey);
        
        int prefix = type.getValue();
        byte[] keyHash = new byte[1 + hash160.length];
        keyHash[0] = (byte)prefix;
        System.arraycopy(hash160, 0, keyHash, 1, hash160.length);

        byte[] checksum = checksum(keyHash);

        byte[] raw = new byte[keyHash.length + checksum.length];
        System.arraycopy(keyHash, 0, raw, 0, keyHash.length);
        System.arraycopy(checksum, 0, raw, keyHash.length, checksum.length);

        return Base58.encode(raw);
    }

    /**
     * Decode chain address to hash
     *
     * @param address - address id
     * @return 20-byte hash for this address
     */
    private static byte[] decodeAddress(String address) {
        byte[] addrRaw = Base58.decode(address);
        if (addrRaw.length != (21+4) ) {
            return null;
        }

        byte[] hash160 = new byte[20];
        System.arraycopy(addrRaw, 1, hash160, 0, hash160.length);
        return hash160;
    }

    /**
     * Validate chain address
     *
     * @param address - address id
     * @return true/false
     */
    public static Boolean validateAddress(String address) {
        byte[] addrRaw = Base58.decode(address);
        if (addrRaw.length != (21+4) ) {
            return false;
        }

        byte[] keyHash = new byte[21];
        byte[] checksum = new byte[4];
        System.arraycopy(addrRaw, 0, keyHash, 0, keyHash.length);
        System.arraycopy(addrRaw, keyHash.length, checksum, 0, checksum.length);

        byte[] verify = checksum(keyHash);
        return Arrays.equals(checksum, verify);
    }


    /**
     * Verify chain address against public key
     *
     * @param address - address id
     * @param publicKey - publicKey
     * @return true/false
     */
    public static Boolean verifyAddress(String address, byte[] publicKey) {
        byte[] hash160 = decodeAddress(address);

        byte[] h = hash160(publicKey);

        return Arrays.equals(hash160, h);
    }

    /**
     * Encode private key into Wif
     *
     * @param privateKey - private key
     * @return base58 encoded string
     */
    public static String encodeWif(byte[] privateKey) {
        byte[] x = new byte[1 + privateKey.length];
        x[0] = VERSION_PRIVATE_KEY;
        System.arraycopy(privateKey, 0, x, 1, privateKey.length);

        byte[] checksum = checksum(x);

        byte[] raw = new byte[ x.length + checksum.length];
        System.arraycopy(x, 0, raw, 0, x.length);
        System.arraycopy(checksum, 0, raw, x.length, checksum.length);

        return Base58.encode(raw);
    }

    /**
     * Decode wif into private key
     *
     * @param wif - wif
     * @return private key
     */
    public static byte[] decodeWif(String wif) {
        byte[] raw = Base58.decode(wif);

        byte[] privateKey = new byte[raw.length -1 - 4];
        System.arraycopy(raw, 1, privateKey, 0, privateKey.length);

        return privateKey;
    }

    /**
     * generate nonce
     *
     * @return 8-byte random array
     */
    public static byte[] nonce() {
        return ByteUtil.generateRandomArray(8);
    }

    /**
     * Generate a random chain address.
     *
     * @param type -address type
     * @return address
     */
    public static Address generateAddress(AddressPrefix type) {

        KeyPair pair = PublicKeyUtil.generateKeyPair();

        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();

        // Get the bytes of the public and private keys
        byte[] privateKeyBytes = privateKey.getEncoded();
        byte[] publicKeyBytes = publicKey.getEncoded();

        Address addr = new Address();
        addr.id = encodeAddress(type, publicKeyBytes);
        addr.wif = encodeWif(privateKeyBytes);
        addr.publicKey = publicKeyBytes;
        addr.privateKey = privateKeyBytes;

        return addr;
    }

    /**
     * Generate a chain address given type and private key
     *
     * @param type -address type
     * @param privateKeyBytes -private key
     * @return address
     */
    public static Address generateAddress(AddressPrefix type, byte[] privateKeyBytes) {
        PrivateKey privateKey = PublicKeyUtil.bytesToPrivateKey(privateKeyBytes);
        if (privateKey == null) {
            return null;
        }

        PublicKey publicKey = PublicKeyUtil.generatePubKeyFromPrivKey(privateKey);
        if (publicKey == null) {
            return null;
        }

        // Get the bytes of the public and private keys
        //byte[] privateKeyBytes = privateKey.getEncoded();
        byte[] publicKeyBytes = publicKey.getEncoded();

        Address addr = new Address();
        addr.id = encodeAddress(type, publicKeyBytes);
        addr.wif = encodeWif(privateKeyBytes);
        addr.publicKey = publicKeyBytes;
        addr.privateKey = privateKeyBytes;

        return addr;
    }


    /**
     * sign clear text with private key
     *
     * @param clearText -plain data
     * @param privateKeyBytes -private key
     * @return signature
     */
    public static byte[] sign(byte[] clearText, byte[] privateKeyBytes) {

        PrivateKey privateKey = PublicKeyUtil.bytesToPrivateKey(privateKeyBytes);
        if (privateKey == null) {
            return null;
        }

        return PublicKeyUtil.sign(clearText, privateKey);
    }

    /**
     * sign hash with private key
     *
     * @param hash -plain data
     * @param privateKeyBytes -private key
     * @return signature
     */
    public static byte[] signHash(byte[] hash, byte[] privateKeyBytes) {

        return sign(hash, privateKeyBytes);

    }

    /**
     * verify signature against public key
     *
     * @param clearText -plain data
     * @param signature -signature
     * @param publicKeyBytes -public key
     * @return true/false
     */
    public static Boolean verify(byte[] clearText, byte[] signature, byte[] publicKeyBytes) {

        PublicKey publicKey = PublicKeyUtil.bytesToPublicKey(publicKeyBytes);
        if (publicKey == null) {
            return false;
        }

        return PublicKeyUtil.verify(clearText, signature, publicKey);
    }

    /**
     * verify hash data against public key
     *
     * @param hash -plain data
     * @param signature -signature
     * @param publicKeyBytes -public key
     * @return true/false
     */
    public static Boolean verifyHash(byte[] hash, byte[] signature, byte[] publicKeyBytes) {

        return verify(hash, signature, publicKeyBytes);

    }

    /**
     * AES CFB-128 encrypt
     *
     * @param clearText - clear text
     * @param iv_secretKey - IV and secret key in 32 bytes
     * @return encrypted data array
     */
    public static byte[] encrypt(byte[] clearText, byte[] iv_secretKey) {

        if (iv_secretKey.length != 32) {
            return null;
        }

        // split to iv and secretKey
        byte[] iv = new byte[16];
        byte[] secretKey = new byte[16];
        System.arraycopy(iv_secretKey, 0, iv, 0, iv.length);
        System.arraycopy(iv_secretKey, iv.length, secretKey, 0, secretKey.length);

        return BlockCipherUtil.aesCfbEncrypt(iv, secretKey, clearText);
    }

    /**
     * AES CFB-128 decrypt
     *
     * @param cipherText - ciphered data
     * @param iv_secretKey - IV and secret key in 32 bytes
     * @return decrypted data array
     */
    public static byte[] decrypt(byte[] cipherText, byte[] iv_secretKey) {

        if (iv_secretKey.length != 32) {
            return null;
        }

        // split to iv and secretKey
        byte[] iv = new byte[16];
        byte[] secretKey = new byte[16];
        System.arraycopy(iv_secretKey, 0, iv, 0, iv.length);
        System.arraycopy(iv_secretKey, iv.length, secretKey, 0, secretKey.length);

        return BlockCipherUtil.aesCfbDecrypt(iv, secretKey, cipherText);
    }

    /**
     * Derive a key from the shared secret, this is chain specific function
     *
     * @param sharedSecret - shared secret
     * @return HMAC based derived key
     */
    private static byte[] deriveKey(byte[] sharedSecret) {
        try {
            byte[] salt = new byte[32]; // all zero's
            byte[] prk = HmacUtil.hmacSha256(salt, sharedSecret);

            byte[] cnt = new byte[1];
            cnt[0] = 1;

            byte[] h0 = HmacUtil.hmacSha256(prk, cnt);
            return h0;

            // MessageDigest hash = MessageDigest.getInstance("SHA-256");
            // hash.update(sharedSecret);
            // byte[] derivedKey = hash.digest();
            // return derivedKey;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * encrypt data using key derived from ECDH shared secret
     *
     * @param clearText - plain data
     * @param theirPublicKeyBytes - other public key
     * @param myPrivateKeyBytes - my private key
     * @return encrypted data
     */
    public static byte[] encrypt(byte[] clearText, byte[] theirPublicKeyBytes, byte[] myPrivateKeyBytes) {
        // ecdh to compute shared secret
        PrivateKey myPrivateKey = PublicKeyUtil.bytesToPrivateKey(myPrivateKeyBytes);
        PublicKey otherPublicKey = PublicKeyUtil.bytesToPublicKey(theirPublicKeyBytes);
        byte[] sharedSecret = PublicKeyUtil.computeKey(myPrivateKey, otherPublicKey);

        // hkdf derive key from shared secret
        byte[] derivedKey = deriveKey(sharedSecret);

        // aes cfb128 encryption
        return encrypt(clearText, derivedKey);

        //return ByteUtil.stringToBytes(ByteUtil.bytesToHexString(clearText)); // dummy
    }

    /**
     * decrypt data using key derived from ECDH shared secret
     *
     * @param cipherText - encrypted data
     * @param theirPublicKeyBytes - other public key
     * @param myPrivateKeyBytes - my private key
     * @return decrypted data
     */
    public static byte[] decrypt(byte[] cipherText, byte[] theirPublicKeyBytes, byte[] myPrivateKeyBytes) {
        // ecdh to compute shared secret
        PrivateKey myPrivateKey = PublicKeyUtil.bytesToPrivateKey(myPrivateKeyBytes);
        PublicKey otherPublicKey = PublicKeyUtil.bytesToPublicKey(theirPublicKeyBytes);
        byte[] sharedSecret = PublicKeyUtil.computeKey(myPrivateKey, otherPublicKey);

        // hkdf derive key from shared secret
        byte[] derivedKey = deriveKey(sharedSecret);

        // aes cfb128 decryption
        return decrypt(cipherText, derivedKey);

        //return ByteUtil.hexStringToBytes(ByteUtil.bytesToString(cipherText)); // dummy
    }

    /**
     * encrypt the key using ECDH shared key
     *
     * @param keyData - plain data
     * @param theirPublicKeyBytes - other public key
     * @param myPrivateKeyBytes - my private key
     * @return encrypted data
     */
    public static byte[] encryptKey(byte[] keyData, byte[] theirPublicKeyBytes, byte[] myPrivateKeyBytes) {
        // ecdh to compute shared secret
        PrivateKey myPrivateKey = PublicKeyUtil.bytesToPrivateKey(myPrivateKeyBytes);
        PublicKey otherPublicKey = PublicKeyUtil.bytesToPublicKey(theirPublicKeyBytes);
        byte[] sharedSecret = PublicKeyUtil.computeKey(myPrivateKey, otherPublicKey);

        // aes cfb128 encryption
        return encrypt(keyData, sharedSecret);
    }

    /**
     * decrypt the key using ECDH shared key
     *
     * @param keyData - encrypted data
     * @param theirPublicKeyBytes - other public key
     * @param myPrivateKeyBytes - my private key
     * @return decrypted data
     */
    public static byte[] decryptKey(byte[] keyData, byte[] theirPublicKeyBytes, byte[] myPrivateKeyBytes) {
        // ecdh to compute shared secret
        PrivateKey myPrivateKey = PublicKeyUtil.bytesToPrivateKey(myPrivateKeyBytes);
        PublicKey otherPublicKey = PublicKeyUtil.bytesToPublicKey(theirPublicKeyBytes);
        byte[] sharedSecret = PublicKeyUtil.computeKey(myPrivateKey, otherPublicKey);

        // aes cfb128 decryption
        return decrypt(keyData, sharedSecret);
    }

    /**
     * determine if private key is valid
     *
     * @param algorithm - algorithm
     * @param privateKey - private key
     * @return true/false
     */
    public static Boolean isValidPrivateKey(String algorithm, byte[] privateKey) {
        // is valid EC private keys?
        return true;
    }   
}