package com.mastercard.blockchain.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class HashUtil {

    // SHA-1
	public static byte[] sha1(byte[] data) {
		try {
			MessageDigest algo = MessageDigest.getInstance("SHA1");
			byte[] h = algo.digest(data);
			return h;

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

        // SHA-256
	public static byte[] sha256(byte[] data) {
		try {
			MessageDigest algo = MessageDigest.getInstance("SHA-256");
			byte[] h = algo.digest(data);
			return h;

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}



    /**
     * Returns a new SHA-256 MessageDigest instance.
     *
     * This is a convenience method which wraps the checked
     * exception that can never occur with a RuntimeException.
     *
     * @return a new SHA-256 MessageDigest instance
     */
    public static MessageDigest newDigest() {
    	try {
    		return MessageDigest.getInstance("SHA-256");
    	} catch (NoSuchAlgorithmException e) {
             throw new RuntimeException(e);  // Can't happen.
         }
     }


    /**
     * Calculates the SHA-256 hash of the given bytes,
     * and then hashes the resulting hash again.
     *
     * @param input the bytes to hash
     * @return the double-hash (in big-endian order)
     */
    public static byte[] hashTwice(byte[] input) {
    	return hashTwice(input, 0, input.length);
    }

    /**
     * Calculates the SHA-256 hash of the given byte range,
     * and then hashes the resulting hash again.
     *
     * @param input the array containing the bytes to hash
     * @param offset the offset within the array of the bytes to hash
     * @param length the number of bytes to hash
     * @return the double-hash (in big-endian order)
     */
    public static byte[] hashTwice(byte[] input, int offset, int length) {
    	MessageDigest digest = newDigest();
    	digest.update(input, offset, length);
    	return digest.digest(digest.digest());
    }

    /**
     * Calculates the hash of hash on the given byte ranges. This is equivalent to
     * concatenating the two ranges and then passing the result to {@link #hashTwice(byte[])}.
     * 
     * @param input1 - byte array 1
     * @param offset1 - offset 1
     * @param length1 - length 1
     * @param input2 - byte array 2
     * @param offset2 - offset 2
     * @param length2 - length 2
     * @return the double hash
     */
    public static byte[] hashTwice(byte[] input1, int offset1, int length1,
    	byte[] input2, int offset2, int length2) {
    	MessageDigest digest = newDigest();
    	digest.update(input1, offset1, length1);
    	digest.update(input2, offset2, length2);
    	return digest.digest(digest.digest());
    }
}