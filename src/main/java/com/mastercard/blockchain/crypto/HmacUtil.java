package com.mastercard.blockchain.crypto;

// HMAC-based Extract-and-Expand Key Derivation Function (HKDF)
//  @ref https://tools.ietf.org/html/rfc5869

import java.security.MessageDigest;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Mac;

public class HmacUtil {

	public static byte[] hmacSha256(byte[] secretKey, byte[] data) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            byte[] digest = mac.doFinal(data);
            return digest;
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        }

        return null;
    }
}