package com.mastercard.blockchain.crypto;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

import java.util.Arrays;


public class BlockCipherUtil {

    // AES encryption
    public static byte[] aesCfbEncrypt(byte[] iv, byte[] key, byte[] plaintext) {

        try {
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            //println "keyLen=" + key.size()

            Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding"); // "AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

            byte[] encrypted = cipher.doFinal(plaintext);

            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }


    public static byte[] aesCfbDecrypt(byte[] iv, byte[] key, byte[] ciphertext) {

    	try {
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding"); // "AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);

            byte[] decrypted = cipher.doFinal(ciphertext);

            return decrypted;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}