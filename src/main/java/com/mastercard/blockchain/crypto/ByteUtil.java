package com.mastercard.blockchain.crypto;


import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.io.UnsupportedEncodingException;
import javax.xml.bind.DatatypeConverter;

public class ByteUtil {

	public static byte[] generateRandomArray(int len) {
		byte[] data = new byte[len];
		SecureRandom random = new SecureRandom();
		random.nextBytes(data);
		return data;
	}

    public static byte[] stringToBytes(String s) {
        try {
            return s.getBytes("UTF-8");
        }
        catch(UnsupportedEncodingException ex) {
            return null;
        }
    }

    public static String bytesToString(byte[] bytes) {
        try {
            return new String(bytes);
        }
        catch(Exception ex) {
            return null;
        }
    }

    public static String bytesToBase64String(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static byte[] base64StringToBytes(String encoded) {
        return Base64.getDecoder().decode(encoded);
    }

    public static String bytesToHexString(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

    public static byte[] hexStringToBytes(String hexString) {
        return DatatypeConverter.parseHexBinary(hexString);
    }

    public static byte[] integerToBytes(int value)
    {
        return new byte[] {
            (byte)((value >> 24) & 0xFF),
            (byte)((value >> 16) & 0xFF),
            (byte)((value >>  8) & 0xFF),
            (byte)(value & 0xFF)
        };
    }

    public static int bytesToInteger(final byte[] bArray)
    {
        return ((bArray[0] & 0xFF) << 24) | ((bArray[1] & 0xFF) << 16) | ((bArray[2] & 0xFF) << 8) | (bArray[3] & 0xFF);
    }


    public static void arrayCopy(byte[] src, int srcOff, byte[] dest, int destOff, int length) {

        System.arraycopy(src, srcOff, dest, destOff, length);

    }

    public static byte[] copyOfRange(byte[] original, int from, int to) {
        return Arrays.copyOfRange(original, from, to);
    }

    public static boolean arrayEqual(byte[] a, byte[] b) {
        return Arrays.equals(a, b);
    }

}