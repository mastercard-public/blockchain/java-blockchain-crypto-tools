package com.mastercard.blockchain.crypto;

import java.security.KeyPairGenerator;
import java.security.KeyPair;
import java.security.KeyFactory;

import java.security.PrivateKey;
import java.security.PublicKey;

import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;

import java.security.Signature;

import java.security.spec.EllipticCurve;
import java.security.spec.ECPoint;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.SignatureException;

import java.security.SecureRandom;
import java.util.Arrays;
import java.math.BigInteger;

import javax.crypto.SecretKey;
import javax.crypto.KeyAgreement;


public class PublicKeyUtil {

    private static final String ChainKeyAlgorithm = "EC";
    private static final String ChainECGenParameterSpec = "secp256r1"; // "prime256v1"

    private static final String ChainSignAlgorithm = "SHA256withECDSA"; // "SHA1WithRSA"

    public static KeyPair generateKeyPair() {
    	try {
    		KeyPairGenerator    g = KeyPairGenerator.getInstance(ChainKeyAlgorithm);
            ECGenParameterSpec     ecGenSpec = new ECGenParameterSpec(ChainECGenParameterSpec); 
            g.initialize(ecGenSpec, new SecureRandom());

            KeyPair pair = g.generateKeyPair();
            PrivateKey priv = pair.getPrivate();
            PublicKey pub = pair.getPublic();

            return pair;
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static byte[] getPrivateKeyBytes(KeyPair pair) {
        PrivateKey privateKey = pair.getPrivate();
        byte[] privateKeyBytes = privateKey.getEncoded();
        return privateKeyBytes;
    }

    // Get the bytes of the public and private keys
    public static byte[] getPublicKeyBytes(KeyPair pair) {
        PublicKey publicKey = pair.getPublic();
        byte[] publicKeyBytes = publicKey.getEncoded();
        return publicKeyBytes;
    }

    public static PrivateKey bytesToPrivateKey(byte[] privateKeyBytes) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ChainKeyAlgorithm);

            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey privateKey2 = keyFactory.generatePrivate(privateKeySpec);

            return privateKey2;
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static PublicKey bytesToPublicKey(byte[] publicKeyBytes) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ChainKeyAlgorithm);

            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            PublicKey publicKey2 = keyFactory.generatePublic(publicKeySpec);

            return publicKey2;
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    // recover public key from EC private key
    public static PublicKey generatePubKeyFromPrivKey(PrivateKey privateKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ChainKeyAlgorithm);

            //need to assume the Implementation. the interfaces doesn't have all information
            ECPrivateKey ecPriv = (ECPrivateKey)privateKey;
            ECParameterSpec params = ecPriv.getParams();

            // compute point for publick key
            ECPoint generator = params.getGenerator();
            BigInteger[] wCoords = EcCore.multiplyPointA(new BigInteger[] {
                generator.getAffineX(), generator.getAffineY() },
                ecPriv.getS(), params);
            ECPoint w = new ECPoint(wCoords[0], wCoords[1]);

            // create a KeySpec
            ECPublicKeySpec pubSpec = new ECPublicKeySpec(w, params);
            // let keyFactory generate public key then
            PublicKey publicKey = keyFactory.generatePublic(pubSpec);
            return publicKey;
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static byte[] sign(byte[] clearText, PrivateKey privateKey) {
    	try {
    		Signature sig = Signature.getInstance(ChainSignAlgorithm); 
    		sig.initSign(privateKey);

    		sig.update(clearText, 0, clearText.length);
    		byte[] signatureBytes = sig.sign();

    		return signatureBytes;
    	} 
        catch(Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static Boolean verify(byte[] clearText, byte[] signature, PublicKey publicKey) {
    	try {
    		Signature sig = Signature.getInstance(ChainSignAlgorithm); 
    		sig.initVerify(publicKey);
    		sig.update(clearText, 0, clearText.length);

    		return sig.verify(signature);
    	}
    	catch(Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    // Use Diffie-Hellman key agreement to compute shared secret
    public static byte[] computeKey(PrivateKey myPrivateKey, PublicKey otherPublicKey) {
        try {
            KeyAgreement ka = KeyAgreement.getInstance("ECDH");
            ka.init(myPrivateKey);
            ka.doPhase(otherPublicKey, true);
            //SecretKey secretKey = ka.generateSecret("TlsPremasterSecret");
            //return secretKey;
            byte[] secretKey = ka.generateSecret();
            return secretKey;
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}